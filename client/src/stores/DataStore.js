import { extendObservable } from 'mobx';

/**
 * DataStore
 */
class DataStore {
    constructor() {
        extendObservable(this, {
            posts: []
        })
    }
}

export default new DataStore();
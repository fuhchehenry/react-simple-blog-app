import { observer } from 'mobx-react';
import React from 'react';
import './App.css';
import LoginForm from './login/LoginForm';
import PostForm from './post/PostsForm';
import UserStore from './stores/UserStore';
import DataStore from './stores/DataStore';
import SinglePost from './components/SinglePost';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      posts: []
    }
  }

  async componentDidMount() {
    try {
      const usr = JSON.parse(sessionStorage.getItem('busr'));
      let res = await fetch('http://localhost:5000/api/users/isLoggedIn', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
        body: JSON.stringify({
          email: usr.email
        })
      });

      let result = await res.json();

      if (result && result.status === 200) {
        UserStore.loading = false;
        UserStore.isLoggedIn = true;
        UserStore.username = result.data.username;
      } else {
        UserStore.loading = false;
        UserStore.isLoggedIn = false;
      }
    }
    catch (err) {
      UserStore.loading = false;
      UserStore.isLoggedIn = false;
    }

    try {
      let req = await fetch('http://localhost:5000/api/posts/', {
        method: 'get',
      });

      const res = await req.json();

      if (res && res.status === 200) {
        DataStore.posts = res.data;
      }
    } catch (error) { }
  }

  render() {

    if (UserStore.loading) {
      return (
        <div className="app">
          <div className="container">
            Loading please wait...
          </div>
        </div>
      )
    } else {

      if (UserStore.isLoggedIn) {
        return (
          <div className="app">
            <div className="container">
              New Blog Post

              <PostForm />

              <h5 className="postsTitle">Blog Posts</h5>

              <div>
                {DataStore.posts.map((post, key) =>
                  <SinglePost post={post} key={key}/>
                )}
              </div>
            </div>
          </div>
        );
      }
      return (
        <div className="app">
          <div className="container">
            <LoginForm />
          </div>
        </div>
      );
    }
  }
}

export default observer(App);

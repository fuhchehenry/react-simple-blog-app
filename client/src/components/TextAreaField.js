import React from '../../node_modules/react';

class TextAreaField extends React.Component {
  render() {
    return (
      <div className="inputField">
        <textarea
        className="textArea"
        rows={this.props.rows}
        placeholder={this.props.placeholder}
        value={this.props.value}
        onChange={ (e) => this.props.onChange(e.target.value)}
        />
      </div>
    );
  }
}

export default TextAreaField;

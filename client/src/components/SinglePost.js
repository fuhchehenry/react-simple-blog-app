import React from '../../node_modules/react';

class SinglePost extends React.Component {

    render() {
        return (
            <div className="postForm">
                <div className="postWrapper">
                    <div className="postHeader">
                        <h5>{this.props.post.title}</h5>
                        <h6><b>Author</b>: {this.props.post.author.username}</h6>
                        <h6>Published On: {new Intl.DateTimeFormat('en-GB').format(new Date(this.props.post.created_at))}</h6>
                    </div>
                    <div className="postContent">
                        <p>{this.props.post.content}</p>
                        <a className="readMore" href="http://localhost:3000/">Read more...</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default SinglePost;

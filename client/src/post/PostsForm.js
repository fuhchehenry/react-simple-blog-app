import React from '../../node_modules/react';
import TextAreaField from '../components/TextAreaField';
import InputField from '../components/InputField';
import SubmitButton from '../components/SubmitButton';


class PostForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      content: '',
      buttonDisabled: false
    }
  }

  setInputValue(property, val) {
    val = val.trim();
    if (val.lenght > 12) {
      return;
    }
    this.setState({
      [property]: val
    });
  }

  resetForm() {
    this.setState({
      title: '',
      content: '',
      buttonDisabled: false
    });
  }

  async onPublish() {
    if (!this.state.title) {
      return;
    }
    if (!this.state.content) {
      return;
    }
    this.setState({
      buttonDisabled: true
    });

    try {
      const usr = JSON.parse(sessionStorage.getItem('busr'));
      delete usr.isLogged;
      delete usr.password;
      let res = await fetch('http://localhost:5000/api/posts/add', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
        body: JSON.stringify({
          title: this.state.title,
          content: this.state.content,
          author: usr
        })
      });

      let result = await res.json();
      if (result && result.status === 200) {
        alert('Your Article has been successfully published');
      } else {
        this.resetForm();
        alert(result.message);
      }

    } catch (error) {
      this.resetForm();
      alert('There was an unexpected error!');
    }
  }

  render() {
    return (
      <div className="postForm">
        <InputField
          type="text"
          placeholder="Title"
          value={this.state.title ? this.state.title : ''}
          onChange={(val) => this.setInputValue('title', val)}
        />

        <TextAreaField
          rows="2"
          placeholder="Body"
          value={this.state.content ? this.state.content : ''}
          onChange={(val) => this.setInputValue('content', val)}
        />

        <SubmitButton
          text="Publish"
          disabled={this.state.buttonDisabled}
          onClick={() => this.onPublish()}
        />
      </div>
    );
  }
}

export default PostForm;

import React from '../../node_modules/react';
import InputField from '../components/InputField';
import UserStore from '../stores/UserStore';
import SubmitButton from '../components/SubmitButton';


class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      buttonDisabled: false
    }
  }

  setInputValue(property, val) {
    val = val.trim();
    // if (val.length > 12) {
    //   return;
    // }
    this.setState({
      [property]: val
    });
  }

  resetForm() {
    this.setState({
      email: '',
      password: '',
      buttonDisabled: false
    });
  }

  async onLogin() {
    if (!this.state.email) {
      return;
    }
    if (!this.state.password) {
      return;
    }
    this.setState({
      buttonDisabled: true
    });

    try {
      let res = await fetch('http://localhost:5000/api/users/login', {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password
        })
      });

      let result = await res.json();
      if (result && result.status === 200) {
        sessionStorage.setItem('busr', JSON.stringify(result.data));
        UserStore.isLogged = true;
        UserStore.email = result.data.email;
        UserStore.username = result.data.username;
        window.location.reload();
      } else {
        this.resetForm();
        alert(result.message);
      }

    } catch (error) {
      console.log(error)
      this.resetForm();
      alert('There was an unexpected error!');
    }
  }

  render() {
    return (
      <div className="loginForm">
        <h5 className="loginTitle">Login Form</h5>
        <InputField
          type="email"
          placeholder="Email"
          value={this.state.email ? this.state.email : ''}
          onChange={(val) => this.setInputValue('email', val)}
        />

        <InputField
          type="password"
          placeholder="Password"
          value={this.state.password ? this.state.password : ''}
          onChange={(val) => this.setInputValue('password', val)}
        />

        <SubmitButton
          text="Login"
          disabled={this.state.buttonDisabled}
          onClick={() => this.onLogin()}
        />

      </div>
    );
  }
}

export default LoginForm;

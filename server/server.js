require('./config/db');

const express = require('express');
const cors = require('cors');
const body_parser = require('body-parser');

const post_ctrl = require('./app/controllers/post.controller');
const user_ctrl = require('./app/controllers/user.controller');

const app = express();

// Defining Server Port
const port = process.env.PORT || 5000;

// Initialize cors Handlers
app.use(cors());

// Initializing BodyParser
app.use(body_parser.json());

app.use('/api/posts', post_ctrl);
app.use('/api/users', user_ctrl);

// Adding Requests Listener on configured Port
app.listen(port, () => {
    console.log('Express Server Started on Port ', port);
});
const express = require('express');
const service = require('../services/post.service');

const router = express.Router();

router.get('/', (req, res) => {
    service.findAll(req, res);
});

router.post('/add', (req, res) => {
    service.create(req, res);
});

router.put('/update/:id', (req, res) => {
    service.update(req, res);
});

router.delete('/delete/:id', (req, res) => {
    service.delete(req, res);
});

module.exports = router;
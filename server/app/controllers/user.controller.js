const express = require('express');
const service = require('../services/user.service');

const router = express.Router();

router.post('/login', (req, res) => {
    service.login(req, res);
});

router.post('/register', (req, res) => {
    service.register(req, res);
});

router.post('/isLoggedIn', (req, res) => {
    service.isLoggedIn(req, res);
});

module.exports = router;
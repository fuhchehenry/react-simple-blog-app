const mg = require('mongoose');

let user_schema = new mg.Schema({
    email: { type: String, index: { unique: true} },
    username: { type: String },
    password: { type: String, required: true },
    isLogged: { type: Boolean, default: false }
});

const User = module.exports = mg.model('User', user_schema);
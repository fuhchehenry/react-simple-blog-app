const mg = require('mongoose');

let post_schema = new mg.Schema({
    title: {
        type: String,
        required: true 
    },
    content: {
        type: String,
        required: true
    },
    author: {
        type: Object,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

const Post = module.exports = mg.model('Post', post_schema);
const User = require('../models/user.model');
const m = {};

m.login = function (req, res) {
    const email = req.body.email;
    const pwd = req.body.password;
    const query = { email: email, password: pwd };

    User.findOne(query)
        .then((user) => {
            if (user != null && user != undefined) {
                user.isLogged = true;
                user.save().then(usr => {
                    usr.password = null;
                    res.send({ status: 200, message: "Login Successful", data: usr });
                }).catch(err => {
                    res.send({ status: 500, message: err, data: null });
                });
            } else {
                res.send({ status: 404, message: "User Not Found: Email or Password Incorrect", data: null });
            }
        })
        .catch((err) => {
            res.send({ status: 500, message: err, data: null });
        });
}

m.register = function (req, res) {
    const nUser = new User({
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    });
    nUser.save()
        .then((user) => {
            user.password = null;
            res.send({ status: 200, message: "User Account successfully created", data: user });
        })
        .catch(err => {
            res.send({ status: 500, message: err, data: null });
        });
}

m.isLoggedIn = function (req, res) {
    const email = req.body.email;
    const query = { email: email };

    User.findOne(query)
        .then((user) => {
            if (user != null && user != undefined && user.isLogged) {
                user.password = null;
                res.send({ status: 200, message: "User Session Exists", data: user });
            } else {
                res.send({ status: 404, message: "User Not Found: Email or Password Incorrect", data: null });
            }
        })
        .catch((err) => {
            res.send({ status: 500, message: err, data: null });
        });

}

module.exports = m;
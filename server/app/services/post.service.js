const Post = require('../models/post.model');
const m = {};

m.findAll = function (req, res) {
    Post.find().sort({created_at: -1})
        .then((posts) => {
            res.send({ status: 200, message: null, data: posts });
        })
        .catch(err => {
            res.send({ status: 500, message: err, data: null });
        });
}

m.create = function (req, res) {
    const nPost = new Post({
        title: req.body.title,
        content: req.body.content,
        author: req.body.author
    });
    nPost.save()
        .then((post) => {
            res.send({ status: 200, message: "Post successfully created", data: post });
        })
        .catch(err => {
            res.send({ status: 500, message: err, data: null });
        });
}

m.update = function (req, res) {
    const id = req.params.id;
    Post.findById(id)
    .then(post => {
        post.title = req.body.title;
        post.content = req.body.content;

        post.save()
        .then((post) => {
            res.send({ status: 200, message: "Post successfully updated", data: post });
        })
        .catch((err) => {
            res.send({ status: 500, message: err, data: null });
        })
    })
    .catch(err => {
        res.send({ status: 500, message: err, data: null });
    });
}

m.delete = function(req, res) {
    const id = req.params.id;
    Post.findById(id)
    .then(post => {
        post.delete()
        .then((post) => {
            res.send({ status: 200, message: "Post successfully deleted", data: post });
        })
        .catch((err) => {
            res.send({ status: 500, message: err, data: null });
        })
    })
    .catch(err => {
        res.send({ status: 500, message: err, data: null });
    });
}

module.exports = m;
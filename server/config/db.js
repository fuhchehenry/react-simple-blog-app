const mongoose = require('mongoose');

const db = require('./db_params').database;

mongoose.connect(db, { useNewUrlParser: true})
        .then(() => {
            console.log("Database Connected Successfully");
        })
        .catch(() => {
            console.log("Unable to establish connection with database", err);
        });

/**
 * Import Models
 */
require('../app/models/post.model');
require('../app/models/user.model');